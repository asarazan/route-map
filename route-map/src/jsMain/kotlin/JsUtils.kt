import net.sarazan.routemap.IRouteMap
import net.sarazan.routemap.RouteMap

internal fun <V : Any> createRouteMap(json: dynamic = null): IRouteMap<V> {
    if (json == null) {
        return RouteMap()
    }
    val keys = keys(json).map {
        Pair(it, json[it])
    }.toTypedArray()
    return RouteMap(*keys)
}

internal fun keys(json: dynamic): Array<String> = js("Object").keys(json).unsafeCast<Array<String>>()

internal fun<K, V> Map<K,V>.toDynamic(): dynamic {
    val retval: dynamic = object {}
    this.forEach {
        retval[it.key] = it.value
    }
    return retval
}