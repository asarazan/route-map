import net.sarazan.routemap.IRouteMap

/**
 * The main entry point for JS clients.
 * Just call new RouteMap() or new RouteMap({...})
 */
internal class JsRouteMap<V : Any>
private constructor(private val common: IRouteMap<V>)
    : IRouteMap<V> by common
{
    @JsName("RouteMap")
    internal constructor(json: dynamic = null) : this(createRouteMap(json))
    override fun get(route: String): IRouteMap.Entry<V>? {
        val base = common[route]
        return base?.let {
            @Suppress("UnsafeCastFromDynamic")
            it.copy(parameters = it.parameters.toDynamic())
        }
    }
}

@JsName("routeMapOf")
internal fun <V : Any> routeMapOfDynamic(json: dynamic = null): IRouteMap<V> {
    return JsRouteMap(json)
}