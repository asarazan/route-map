/**
 * We can't currently run these because the package name mangling happening for npm.
 */

//import kotlin.test.Test
//import kotlin.test.assertEquals
//
//class JsRouteMapTests {
//
//    @Test
//    fun testParamsAreDynamic() {
//        val map = JsRouteMap<String>()
//        map["/users/{uid}"] = "bar"
//        val entry = map["/users/foo"]
//        val params = entry?.parameters
//        val json = JSON.stringify(params)
//        assertEquals("{\"uid\":\"foo\"}", json)
//    }
//
//}