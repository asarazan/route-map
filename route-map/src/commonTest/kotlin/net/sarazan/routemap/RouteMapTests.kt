package net.sarazan.routemap

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertNull

class RouteMapTests {

    @Test
    fun testBasicGet() {
        val map = routeMapOf("foo" to 1)
        assertEquals(1, map["foo"]!!.value)
    }

    @Test
    fun testNull() {
        val map = routeMapOf<Int>()
        assertEquals(null, map["foo"])
    }

    @Test
    fun testBasicWildcard() {
        var map = routeMapOf("/users/*" to 1)
        assertFalse(map.containsKey("/users"))
        assertFalse(map.containsKey("/users/1/2"))
        assertFalse(map.containsKey("/users1"))
        assertEquals(1, map["/users/1"]!!.value)
        assertEquals(1, map["/users/2"]!!.value)

        map = routeMapOf("/users*" to 1)
        assertEquals(1, map["/users1"]!!.value)
        assertFalse(map.containsKey("/users/1"))
    }

    @Test
    fun testDoubleWildcard() {
        val map = routeMapOf("/users/**" to 1)
        assertEquals(1, map["/users/1"]!!.value)
        assertEquals(1, map["/users/1/2"]!!.value)
        assertEquals(1, map["/users/1/22345$#()FD(@#$$^$#"]!!.value)
        assertEquals(null, map["/users"])
    }

    @Test
    fun testParamPattern() {
        val map = routeMapOf("/users/{userId}" to 1)
        assertFalse(map.containsKey("/users"))
        assertFalse(map.containsKey("/users/1/2"))
        assertFalse(map.containsKey("/users1"))
        assertEquals(1, map["/users/1"]!!.value)
        assertEquals(1, map["/users/2"]!!.value)
    }

    @Test
    fun testSingleParam() {
        val map = routeMapOf("/users/{userId}" to 1)
        assertEquals(mapOf("userId" to "1"), map["/users/1"]!!.parameters)
    }

    @Test
    fun testMultiParams() {
        val map = routeMapOf("/users/{userId}/something/{verb}" to 1)
        assertEquals(mapOf("userId" to "1", "verb" to "foo"), map["/users/1/something/foo"]!!.parameters)
    }

    @Test
    fun testRanking() {
        val map = routeMapOf("/users/*" to 1, "/users/foo" to 2)
        assertEquals(1, map["/users/bar"]!!.value)
        assertEquals(2, map["/users/foo"]!!.value)
    }

    @Test
    fun testRankingFromKtorDocs() {
        val map = routeMapOf("/{user}" to 1, "/settings" to 2)
        assertEquals(1, map["/kotlin"]!!.value)
        assertEquals(2, map["/settings"]!!.value)
    }
}