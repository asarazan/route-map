package net.sarazan.routemap

import net.sarazan.routemap.util.matchToParams
import net.sarazan.routemap.util.regexify
import net.sarazan.routemap.util.sortMatches
import kotlin.js.JsName

fun<V: Any> routeMapOf(vararg pairs: Pair<String, V>) = RouteMap(*pairs)

@JsName("FooBar")
class RouteMap<V : Any>(vararg pairs: Pair<String, V>) : IRouteMap<V>
{
    private data class EntryInternal<V : Any>(val value: V, val route: String, val params: List<String>)
    private val routes = linkedMapOf<String, EntryInternal<V>>()

    init {
        pairs.forEach {
            set(it.first, it.second)
        }
    }

    override operator fun get(route: String): IRouteMap.Entry<V>? {
        val matches = routes.keys.mapNotNull {
            k ->
            Regex(k).matchEntire(route)?.let {
                val value = routes[k]!!
                IRouteMap.Entry(value.value, value.route,
                    matchToParams(it, value.params)
                )
            }
        }
        return sortMatches(route, matches).firstOrNull()
    }

    override operator fun set(route: String, value: V?) {
        val re = regexify(route)
        routes[re.first] = EntryInternal(value as V, route, re.second)
    }

    override fun remove(route: String) {
        set(route, null)
    }

    override fun containsKey(route: String): Boolean {
        return get(route) != null
    }

    override fun containsValue(value: V): Boolean {
        return routes.values.find {
            it.value == value
        } != null
    }
}