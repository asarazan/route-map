package net.sarazan.routemap.util

import net.sarazan.routemap.IRouteMap

internal val findStar = Regex("\\*(?<!\\.\\*)")
internal const val replaceStar = "[^/]*"

internal val findStar2 = Regex("\\*\\*.*")
internal const val replaceStar2 = ".*"

internal val findParam = Regex("\\{([^}]*)\\}")
internal const val replaceParam = "($replaceStar)"

internal fun regexify(query: String): Pair<String, List<String>> {
    val params = findParam.findAll(query).map {
        it.groupValues[1]
    }.toList()
    val string = query
        .replace(
            findStar2,
            replaceStar2
        )
        .replace("/", "\\/")
        .replace(
            findStar,
            replaceStar
        )
        .replace(
            findParam,
            replaceParam
        )
    return Pair(string, params)
}

internal fun matchToParams(match: MatchResult, querys: List<String>): Map<String, String> {
    val pairs = querys.mapIndexed { i, k ->
        Pair(k, match.groupValues[i + 1])
    }
    return linkedMapOf(*pairs.toTypedArray())
}

internal fun<V : Any> sortMatches(query: String, matches: List<IRouteMap.Entry<V>>): List<IRouteMap.Entry<V>> {
    return matches.sortedBy {
        val route = it.route
            .replace(findStar2, "")
            .replace(findStar, "")
            .replace(findParam, "")
        levenshtein(route, query)
    }
}