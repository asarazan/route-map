package net.sarazan.routemap

import kotlin.js.JsName

interface IRouteMap<V : Any> {

    @JsName("RouteMapEntry")
    data class Entry<V : Any>(

        @JsName("value")
        val value: V,

        @JsName("route")
        val route: String,

        @JsName("parameters")
        val parameters: Map<String, String> = linkedMapOf()
    )

    @JsName("get")
    operator fun get(route: String): Entry<V>?

    @JsName("set")
    operator fun set(route: String, value: V?)

    @JsName("remove")
    fun remove(route: String)

    @JsName("hasRoute")
    fun containsKey(route: String): Boolean

    @JsName("hasValue")
    fun containsValue(value: V): Boolean
}