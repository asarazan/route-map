export declare interface RouteMapEntry<V> {
    readonly value: V;
    readonly route: string;
    readonly parameters: Record<string, string>;
}

export declare class RouteMap<V> {
    public constructor(map?: Record<string, V>);
    public get(route: string): RouteMapEntry<V> | undefined;
    public set(route: string, value: V | undefined): void;
    public remove(route: string): void;
    public hasRoute(route: string): boolean;
    public hasValue(value: V): boolean;
}

export declare function routeMapOf<V>(map?: Record<string, V>): RouteMap<V>;